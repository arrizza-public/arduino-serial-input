# encoding: UTF-8
# frozen_string_literal: true
# ============================================================================
# Summary: run some tests against ArduinoSerialInput to check for
#          handling of serial input from a script
# ============================================================================
require 'serialport'

# --------------------
def read_response
  loop do
    # get the next byte
    ch = @mc.getbyte

    # if we got nothing from the Arduino,
    # it may have timed out, try again
    next if ch.nil?

    # the response ends on a linefeed from the Arduino
    if ch == 0x0A
      puts
      return
    end

    # print the character as a character (not a byte) on your PC
    print(ch.chr)
  end
end

# >> MAIN --------------------
# open the port with 115200 81N
@port            = '/dev/ttyUSB0'
parity           = SerialPort::NONE
@mc              = SerialPort.new(@port, 115_200, 8, 1, parity)

# temporarily set the read timeout to non-blocking
@mc.read_timeout = -1
@mc.flush_input
@mc.flush_output

# read a couple of times to get rid of garbage
@mc.getbyte
@mc.getbyte

# reset the read_time to 1s
@mc.read_timeout = 1000

s = 'Hello World!'
s.each_byte do |ch|
  @mc.putc(ch)
  read_response
end

@mc.close
