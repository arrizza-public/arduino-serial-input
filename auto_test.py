import serial
import time

from tools.xplat_utils.os_specific import OsSpecific


# --------------------
## automate testing of the arduino app
class App:
    # --------------------
    ## constructor
    def __init__(self):
        ## reference to the serial port
        self._ser = None

    # --------------------
    ## initialize
    #
    # @return None
    def init(self):
        OsSpecific.init()
        if OsSpecific.os_name == 'macos':
            port = '/dev/tty.usbserial-113240'
        else:
            port = '/dev/ttyUSB0'
        self._ser = serial.Serial(port, 115_200)

    # --------------------
    ## terminate
    #
    # @return None
    def term(self):
        print()

        # close it
        self._ser.close()

    # --------------------
    ## send a message to the arduino and get the response back.
    # should just echo the incoming message
    #
    # @return None
    def run(self):
        self._read_response()

        msg = 'Hello World!'
        for ch in msg:
            self._ser.write(ch.encode('utf-8'))
            time.sleep(0.1)
            self._read_response()

        ch = 'b'  # blink 3 times
        self._ser.write(ch.encode('utf-8'))
        for _ in range(1 + 3):  # 1 for the 'b' and 3 lines for the blinks
            self._read_response()

    # --------------------
    ## Read response from arduino
    #
    # @return None
    def _read_response(self):
        while True:
            # get the next byte
            ch = self._ser.read(size=1)

            # if we got nothing from the Arduino,
            # it may have timed out, try again
            if not ch:
                continue

            # the response ends on a linefeed from the Arduino
            if ch == b'\r':
                break

            # print the character as a character (not a byte) on your PC
            print(ch.decode('utf-8'), end='')


# --------------------
### MAIN
def main():
    app = App()
    app.init()
    app.run()
    app.term()


main()
