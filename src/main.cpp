// cppcheck-suppress missingInclude
#include <Arduino.h>
#include <ctype.h>

// --------------------
//! Arduino setup
// cppcheck-suppress unusedFunction
void setup()
{
    // open the serial port at 115200 bps
    Serial.begin(115200);

    // tell the world we are setting up
    Serial.println("serial input");

    // initialize the digital pin as an output.
    // Pin 13 has an LED connected on most Arduino boards:
    pinMode(13, OUTPUT);

    // ensure the LED is off
    digitalWrite(13, LOW);
}

// the character read from the Serial Port
char ch = 0;

// --------------------
//! Arduino main loop
// accept a character from the serial port
// for all characters print the hex and decimal value of the character
// and then the character
// if it's a 'b', blink the LED rapidly 3 times
// cppcheck-suppress unusedFunction
void loop()
{
    // if no data is available skip the rest of the code
    if (Serial.available() <= 0) {
        return;
    }

    // read the incoming byte:
    ch = Serial.read();

    // say what you got:
    Serial.print("recv: 0x");
    Serial.print(ch, HEX);
    Serial.print("=");
    Serial.print(ch, DEC);
    Serial.print("=");
    Serial.println(ch);

    int num_blinks = 0;
    if (ch == 'b') {
        num_blinks = 3;
    }

    if (isdigit(ch)) {
        num_blinks = ch - '0';
    }

    // blink the LED
    for (int i = 0; i < num_blinks; ++i) {
        Serial.print("blink ");
        Serial.print(i + 1, DEC);
        Serial.print(" of ");
        Serial.println(num_blinks, DEC);
        digitalWrite(13, HIGH);
        delay(200);
        digitalWrite(13, LOW);
        delay(200);
    }
}
